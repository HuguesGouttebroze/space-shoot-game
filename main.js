/* UTILS FUNCTIONS */

function getRandomInteger(min, max){
  return Math.floor(Math.random() * (max - min + 1) + min);
}

function getRandomRGBA(){
  let r = getRandomInteger(0, 255);
  let g = getRandomInteger(0, 255);
  let b = getRandomInteger(0, 255);
  let a = Math.random();
  return `rgba(${r},${g},${b},${a})`;
}

/* PRINCIPAL CODE */

window.addEventListener('DOMContentLoaded', function(){

  const canvas = document.querySelector('#canvas1');
  const ctx = canvas.getContext('2d');

  canvas.width = innerWidth;
  canvas.height = innerHeight;

  class Player {
    constructor() {
      this.position = {
        x: this.width,
        y: this.height
      },

      this.velocity = {
        x: 0,
        y: 0
      }

      this.rotation = 0;

      const image = new Image();
      image.src = 'spaceship.png';
      image.onload = () => {
        const scale = 0.15;
        this.image = image;
        this.width = image.width * scale;
        this.height = image.height * scale;
        this.position = {
          x: canvas.width / 2 - this.width / 2,
          y: canvas.height - this.height - 30
        }
      }
    }

    draw() {
      // ctx.fillStyle ='red';
      // ctx.fillRect(this.position.x, this.position.y, this.width, this.height);     
      ctx.save();
      ctx.translate(
        player.position.x + player.width / 2, 
        player.position.y + player.height / 2
      );
      ctx.rotate(this.rotation);

      ctx.translate(
        -player.position.x - player.width / 2, 
        -player.position.y - player.height / 2
      );

      ctx.drawImage(
        this.image,
        this.position.x, 
        this.position.y, 
        this.width, 
        this.height
      );
      ctx.restore();
    }

    update() {
      if (this.image){
        this.draw();
        this.position.x += this.velocity.x;        
      }
    }
  };

  class Invader {
    constructor({ position }) {
      /* this.position = {
        x: this.width,
        y: this.height
      }, */

      this.velocity = {
        x: 0,
        y: 0
      }

      // this.rotation = 0;

      const image = new Image();
      image.src = 'invader.png';
      image.onload = () => {
        const scale = 1;
        this.image = image;
        this.width = image.width * scale;
        this.height = image.height * scale;
        this.position = {
          x: position.x,
          y: position.y
        }
      }
    }

    draw() {
      // ctx.fillStyle ='red';
      // ctx.fillRect(this.position.x, this.position.y, this.width, this.height);     
      /* ctx.save();
      ctx.translate(
        player.position.x + player.width / 2, 
        player.position.y + player.height / 2
      );
      ctx.rotate(this.rotation);

      ctx.translate(
        -player.position.x - player.width / 2, 
        -player.position.y - player.height / 2
      ); */

      ctx.drawImage(
        this.image,
        this.position.x, 
        this.position.y, 
        this.width, 
        this.height
      );
      // ctx.restore();
    }

    update({ velocity }) {
      if (this.image){
        this.draw();
        this.position.x += velocity.x;        
        this.position.y += velocity.y;        
      }
    }

    shoot(invaderProjectiles){
      invaderProjectiles.push(new InvaderProjectile({
        position: {
          x: this.position.x + this.width / 2,
          y: this.position.y + this.height
        }, 
        velocity: {
          x: 0,
          y: 5
        }
      }));
    }
  };

  class Projectile {
    constructor({ position, velocity }){
      this.position = position;
      this.velocity = velocity;

      this.radius = 4;
    }

    draw(){
      ctx.beginPath();
      ctx.arc(this.position.x, this.position.y, this.radius, 0, Math.PI * 2);
      ctx.fillStyle = 'red';
      ctx.fill();
      ctx.closePath();
    }

    update(){
      this.draw();
      this.position.x += this.velocity.x;
      this.position.y += this.velocity.y;
    }
  }

  class Particle {
    constructor({ position, velocity, radius, color }){
      this.position = position;
      this.velocity = velocity;
      this.radius = radius;
      this.color = color;
      this.opacity = 1;
    }

    draw(){
      ctx.save();
      ctx.globalAlpha = this.opacity;
      ctx.beginPath();
      ctx.arc(this.position.x, this.position.y, this.radius, 0, Math.PI * 2);
      ctx.fillStyle = this.color;
      ctx.fill();
      ctx.closePath();
      ctx.restore();
    }

    update(){
      this.draw();
      this.position.x += this.velocity.x;
      this.position.y += this.velocity.y;
      this.opacity -= 0.01;
    }
  }

  class InvaderProjectile {
    constructor({ position, velocity }){
      this.position = position;
      this.velocity = velocity;

      this.width = 3;
      this.height = 10;
    }

    draw(){
      ctx.fillStyle = 'white';
      ctx.fillRect(this.position.x, 
        this.position.y, 
        this.width, 
        this.height)
    }

    update(){
      this.draw();
      this.position.x += this.velocity.x;
      this.position.y += this.velocity.y;
    }
  }

  class Grid {
    constructor(){
      this.position = {
        x: 0, 
        y: 0
      },

      this.velocity = {
        x: 3, 
        y: 0
      }

      this.invaders = [];

      const rows = Math.floor(Math.random() * 5 + 2); // add 2 to get a min. of 2 rows if the first random nbr (bettween 0 and 5) is equal to 0 
      const columns = Math.floor(Math.random() * 10 + 5); // wanna add a min. of 5 columns of inviders

      this.width = columns * 30;
      for (let i = 0; i < columns; i++) {
        for (let k = 0; k < rows; k++) {
          this.invaders.push(
            new Invader({
              position: {
                x: i * 30,
                y: k * 30
                }
              }
            )
          );
        }
      }
    }
    update(){
      this.position.x += this.velocity.x;
      this.position.y += this.velocity.y;

      this.velocity.y = 0;
      if (this.position.x + this.width >= canvas.width 
        || this.position.x <= 0){
        this.velocity.x = -this.velocity.x;
        this.velocity.y = 30;
      }
    }
  }

  const player = new Player();
  const projectiles = [];
  const grids = [];
  const invaderProjectiles = [];
  const particles = [];

  const keys = {
    h: {
      pressed: false
    }, 
    l: {
      pressed: false
    }, 
    space: {
      pressed: false
    }
  };

  let frames = 0;
  let randomInterval = Math.floor(Math.random() * 500 + 500);
  
  function createParticles({ object, color }) {
    for (let i = 0; i < 15; i++) {             
      particles.push(new Particle({
        position: {
          x: object.position.x + object.width / 2,
          y: object.position.y + object.height / 2
        },
        velocity: {
          x: (Math.random() - 0.5) * 2,
          y: (Math.random() - 0.5) * 2
        },
        radius: Math.random() * 3,
        color: color || '#BAA9DE' // color: 'yellow',
      }));
    }
  }

  function animate(){
    window.requestAnimationFrame(animate);
    ctx.fillStyle = 'black';
    ctx.fillRect(0, 0, canvas.width, canvas.height);
    player.update();
    // invader.update();
    particles.forEach((particle, i) => {
      if (particle.opacity <= 0){
        setTimeout(() => {
          particles.splice(i, 1);
        }, 0);
      } else {
        particle.update();
      }
    })
    invaderProjectiles.forEach((invaderProjectile, index) => {
      if (invaderProjectile.position.y + invaderProjectile.height >= canvas.height){
        setTimeout(() => {
          invaderProjectiles.splice(index, 1);
        }, 0)
      } else invaderProjectile.update();

      if (invaderProjectile.position.y + 
        invaderProjectile.height >= player.position.y && 
        invaderProjectile.position.x + invaderProjectile.width >= 
        player.position.x && invaderProjectile.position.x <= 
        player.position.x + player.width){
        console.log('BOUM! BOUM! BOUM!');
        createParticles({
          object: player,
          color: 'white'
        })
      }
    })
    // console.log(invaderProjectiles);
    projectiles.forEach((projectile, index) => {
      if (projectile.position.y + projectile.radius <= 0){
        setTimeout(() => {
          projectiles.splice(index, 1);
        }, 0)
      } else {
        projectile.update();
      }
    })

    grids.forEach((grid, gridIndex) => {
      grid.update();
      // spawn projectiles
      if (frames % 100 === 0 && 
        grid.invaders.length > 0){
          grid.invaders[Math.floor(Math.random() * grid.invaders.length)].shoot(invaderProjectiles);
      }
      grid.invaders.forEach((invader, i) => {
        invader.update({ velocity: grid.velocity });
        
        // projectiles hit enemies
        projectiles.forEach((projectile, j) => {
          if (projectile.position.y - projectile.radius <= 
              invader.position.y + invader.height && 
            projectile.position.x + projectile.radius >= 
              invader.position.x && 
            projectile.position.x - projectile.radius <= 
              invader.position.x + invader.width && 
            projectile.position.y + projectile.radius >= 
              invader.position.y
            ){
              setTimeout(() => {
                const invaderFound = grid.invaders.find(
                  (invader2) => invader2 === invader
                )
                const projectileFound = projectiles.find(
                  (projectile2) => projectile2 === projectile
                )

                // remove invader & projectile
                if (invaderFound && projectileFound){
                  createParticles({ 
                    object: invader 
                  });
                  /* for (let i = 0; i < 15; i++) {             
                    particles.push(new Particle({
                      position: {
                        x: invader.position.x + invader.width / 2,
                        y: invader.position.y + invader.height / 2
                      },
                      velocity: {
                        x: (Math.random() - 0.5) * 2,
                        y: (Math.random() - 0.5) * 2
                      },
                      radius: Math.random() * 3,
                      // color: 'yellow',
                      color: '#BAA9DE'
                    }));
                  } */
                  grid.invaders.splice(i, 1);
                  projectiles.splice(j, 1);

                  if (grid.invaders.length > 0){
                    const firstInvader = grid.invaders[0];
                    const lastInvader = grid.invaders[grid.invaders.length - 1];

                    grid.width = lastInvader.position.x - 
                      firstInvader.position.x + 
                      lastInvader.width;
                    grid.position.x = firstInvader.position.x;
                  } else {
                    grids.splice(gridIndex, 1);
                  }
                }
              }, 0);
          }
        })
      })
    })

    if (keys.h.pressed && player.position.x >= 0){
      player.velocity.x = -7; // left player movement speed 
      player.rotation = -0.15;
    } else if (keys.l.pressed && player.position.x + player.width <= canvas.width){
      player.velocity.x = 7;
      player.rotation = 0.15;
    } else {
      player.velocity.x = 0;
      player.rotation = 0;
    }

    // Spawning invaders (traduction fr: "apparition des envahisseurs")
    if (frames % randomInterval === 0){
      grids.push(new Grid());
      randomInterval = Math.floor(Math.random() * 500 + 500);
      frames = 0;
      console.log(randomInterval);
    }

    frames++;
  };

  animate();

  window.addEventListener('keydown', ({ key }) => {
    // console.log(key);
    switch (key) {
      case 'h':
        // console.log('left');
        // player.velocity.x = -5;
        keys.h.pressed = true;
        break;
      case 'l':
        // console.log('right');
        keys.l.pressed = true;
        break;
      case ' ':
        //keys.space.pressed = true;
        projectiles.push(new Projectile({
          position: {
            x: player.position.x + player.width / 2, 
            y: player.position.y
          }, 
          velocity: {
            x: 0,
            y: -10
          }
        }))
        break;
    }
  });

  window.addEventListener('keyup', ({ key }) => {
    // console.log(key);
    switch (key) {
      case 'h':
        // console.log('left');
        // player.velocity.x = -5;
        keys.h.pressed = false;
        break;
      case 'l':
        // console.log('right');
        keys.l.pressed = false;
      case ' ':
        //keys.space.pressed = true;
        break;
    }
  });
});