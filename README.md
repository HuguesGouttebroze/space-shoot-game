# SPACE INVADER, a SHOOT-EM-UP

## Code source & CI/CD 
+ repository on `Gitlab`
+ Find app. deployed on `Vercel`, on Continious Deployement after every git push on `Gitlab`, [here](https://space-shoot-hg.vercel.app/) or on the following `url`: `https://space-shoot-hg.vercel.app/`
+ JavaScript vanilla
+ Conception Orienté Objet 

## STEPS
1. Init. a JS application (`vite.js`, `yarn` ...)
    `yarn create vite`
2. Create a player
3. Movement with keyboard
4. Create projectiles 
5. Create enemies (invaders)
6. Create & move invaders grid (with rows & columns)
7. Spawn grids at intervals
8. Shoot invaders
9. Invaders shoot
10. Invaders explotion
